# -*- coding: utf-8 -*-
{
    'name': "Payments Interests Scale",

    'summary': """
        Calculate and add line to last invoice with interests calculated from
        due payments""",

    'description': """
        Calculate and add line to last invoice with interests calculated from
        due payments
    """,

    'author': "Alitux Fábrega",
    'website': "https://delkarukinka.wordpress.com",
    'category': 'Invoicing',
    'version': '0.1',

    'depends': ['base'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/views.xml',
        # 'views/templates.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        'demo/demo.xml',
    ],
}
