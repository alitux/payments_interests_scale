# Payments Interests Scale Addon

## Description
The Payments Interests Scale addon extends the functionalities of the payments interests module in Odoo. It introduces a new table to manage interest scales, providing the ability to define interest rates based on specified amount ranges.

## Features

- **Interest Scale Management**: Introduces a new model, 'Payments Interests Scale', to define interest scales with 'From Amount' and 'To Amount' ranges along with the corresponding 'Interest Amount'.

## Installation
1. Clone this repository to your Odoo instance:
   ```shell
   git clone https://gitlab.com/alitux/payments_interests_scale
    ```
2. Restart the Odoo server.

## Usage

1. Open the 'Interests Scale' in Invoice/Configuration menu to configure interest scales.
2. Define the 'From Amount', 'To Amount', and 'Interest Amount' for each interest scale.
3. These scales will be used in calculating partner interests.

## Contribution

If you wish to contribute to this project, follow these steps:

1. Fork the repository.
2. Create a new branch for your feature or fix: git checkout -b feature/new-feature or git checkout -b bugfix/fix.
3. Make your changes and commit: git commit -m "Added new feature" or git commit -m "Fixed an issue with ..."
4. Push to your branch: git push origin feature/new-feature.
5. Create a Pull Request on GitLab.

## Author

- Alitux Fábrega (alitux@disroot.org)

## License

This project is under the GNU General Public License (GPL).