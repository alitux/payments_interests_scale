# -*- coding: utf-8 -*-
from odoo import models, fields, api
from odoo.exceptions import ValidationError

class PartnerInterests(models.Model):
    _inherit = 'res.partner'

    interest_amount = fields.Float('Interest Amount')

    def interests_compute(self):
        total_debt = 0
        to_amount_list = []
        moves = self.env['account.move']
        scales = self.env['payments_interests_scale.scale']
        domain = [
            ('partner_id','=',self.id),
            ('payment_state','=','not_paid'),
            ('state','=','posted'),
        ]
        moves_ids = moves.search(domain)
        for move in moves_ids:
            total_debt += move.amount_total_signed
        if len(scales.search([])) == 0:
            raise ValidationError("Empty Scale Table")
        if total_debt == 0:
            return (total_debt,0)
        ##IMPORTANT 
        #Search the max value in to_amount.
        #If total_debt is greater than the max value in to_amount, return the max interests
        for escala in scales.search([]): to_amount_list.append((escala.to_amount, escala.interest_amount))
        if total_debt >= max(to_amount_list)[0]: 
            return (total_debt, max(to_amount_list)[1]) 
        scale_record = scales.search([
            ('from_amount', '<=', total_debt),
            ('to_amount', '>=', total_debt)
        ], limit=1)
        
        interest_amount = scale_record.interest_amount if scale_record else 0
        self.env['res.partner'].browse(self.id).write({'interest_amount': interest_amount})
        #Define line to add in invoice
        linea_factura = {
            'name': f"Interés por Mora (Deuda ${total_debt})",
            'price_unit': interest_amount,
            'quantity': 1,
            }
        #Search Last Invoice in draft state
        ultima_factura = self.env['account.move'].search([('partner_id','=',self.id),('state','=','draft')])[-1]
        ultima_factura.write({'invoice_line_ids':[(0,0,linea_factura)]})
        return True