# -*- coding: utf-8 -*-

from odoo import models, fields, api


class PaymentsInterestsScale(models.Model):
    _name = 'payments_interests_scale.scale'
    _description = 'Scale of Interests'

    from_amount = fields.Float('From Amount', required=True)
    to_amount = fields.Float('To Amount', required=True)
    interest_amount = fields.Float('Interest Amount', required=True)


